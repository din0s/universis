import { Injectable } from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute} from '@angular/router';

@Injectable()
export class AdvancedFormsService {

  constructor(private _context: AngularDataContext,
              private _http: HttpClient,
              private _activatedRoute: ActivatedRoute) {
    //
  }

  async loadForm(name: string): Promise<any> {
      return await new Promise((resolve, reject) => {
        this._http.get(`assets/forms/${name}.json`).subscribe( result => {
            // tslint:disable-next-line: no-use-before-declare
            new ServiceUrlPreProcessor(this._context).parse(result);
            new QueryParamsPreProcessor(this._activatedRoute, this._context).parse(result);
            return resolve(result);
        }, error => {
          return reject(error);
        });
      });
  }
}


export class QueryParamsPreProcessor {
  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) {}
  parse(form: any) {
    form.queryParams = this._activatedRoute.snapshot.queryParams;
  }

}

export class ServiceUrlPreProcessor {
  constructor(private _context: AngularDataContext) {}
  parse(form: any) {
    const headers = this._context.getService().getHeaders();
    const serviceHeaders = Object.keys(headers).map(key => {
      return {
        key: key,
        value: headers[key]
      };
    });
    this.parseComponents(form.components, serviceHeaders);
    return form;
  }

  private parseComponents(components: Array<any>, headers: any) {
    components.forEach( component => {
      if (component.data && component.data.url) {
        component.data.url = this._context.getService().resolve(component.data.url);
        component.data.headers = component.data.headers || [];
        component.data.headers.push.apply(component.data.headers, headers);
      }
      if (component.components) {
        return this.parseComponents(component.components, headers);
      }
      if (component.columns) {
        component.columns.forEach( column => {
          if (column.components) {
            this.parseComponents(column.components, headers);
          }
        });
      }
    });
  }

}
