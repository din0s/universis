/* tslint:disable quotemark */
/* tslint:disable max-line-length */
export const en = {
    "Forms": {
        "Submit": "Submit",
        required: '{{field}} is required',
        pattern: '{{field}} does not match the pattern {{pattern}}',
        minLength: '{{field}} must be greater than {{length}} characters.',
        maxLength: '{{field}} must be less than {{length}} characters.',
        min: '{{field}} should not be less than {{min}}.',
        max: '{{field}} should not be more than {{max}}.',
        maxDate: '{{field}} should not contain date after {{- maxDate}}',
        minDate: '{{field}} should not contain date before {{- minDate}}',
        invalid_email: '{{field}} must be a valid email.',
        invalid_url: '{{field}} must be a valid url.',
        invalid_regex: '{{field}} does not match the pattern {{regex}}.',
        invalid_date: '{{field}} is not a valid date.',
        invalid_day: '{{field}} is not a valid day.',
        mask: '{{field}} does not match pattern.',
        complete: 'Submission Complete',
        error: 'Please fix these issues before submit.'
    }
};
/* tslint:enable quotemark */
/* tslint:enable max-line-length */
