import { Component, OnInit } from '@angular/core';
// import { FormioComponent } from 'angular-formio/components/formio/formio.component';

@Component({
  selector: 'app-formio-examples-base',
  templateUrl: './formio-examples-base.component.html',
  styleUrls: ['./formio-examples-base.component.scss']
})
export class FormioExamplesBaseComponent implements OnInit {

  formioTextfieldConfig: any;
  formioEmailConfig: any;
  formioNumberConfig: any;
  formioPhoneNumberConfig: any;
  formioDateConfig: any;
  formioRadioConfig: any;
  formioCheckboxConfig: any;
  formioSelectHtml5Config: any;
  formioSelectChoicesJSConfig: any;
  formioTooltipConfig: any;
  formioDescriptionConfig: any;
  formioLayoutColumnsConfig: any;
  formioLayoutFieldsetConfig: any;
  formioValidationSimpleConfig: any;
  formioValidationNumberConfig:any;
  formioValidationMinMaxNumberConfig: any;
  formioValidationMaskNumberConfig: any;
  formioValidationRegExpConfig: any;
  formioValidationCustomConfig: any;
  formioValidateDisabledInput: any;
  formioStyleCustomClassConfig: any;
  
  constructor() { }

  ngOnInit() {

    this.formioTextfieldConfig  = {
      "components": [
        {
          "label": "Textfield Label",
          "labelPosition": "top",
          "placeholder": "This is a textfield",
          "widget": {
            "type": "input"
          },
          "validate": {
            "required": false
          },
          "key": "model-property-name",
          "type": "textfield",
          "input": true
        }
      ]
    }

    this.formioEmailConfig  = {
      "components": [
        {
          "label": "Email Label",
          "labelPosition": "top",
          "placeholder": "This is an email field",
          "widget": {
            "type": "input"
          },
          "validate": {
            "required": false
          },
          "key": "model-property-name",
          "type": "email",
          "input": true
        }
      ]
    }

     this.formioNumberConfig  = {
      "components": [
        {
          "label": "Number Label",
          "labelPosition": "top",
          "placeholder": "This is a number field (only numbers allowed)",
          "widget": {
            "type": "input"
          },
          "validate": {
            "required": false
          },
          "key": "model-property-name",
          "type": "number",
          "input": true
        }
      ]
    }

    this.formioPhoneNumberConfig  = {
      "components": [
        {
          "label": "Phone Number Label",
          "labelPosition": "top",
          "inputMask": "+999999999999",
          "placeholder": "This is a phone number field (+302310123456)",
          "widget": {
            "type": "input"
          },
          "validate": {
            "required": false
          },
          "key": "model-property-name",
          "type": "phoneNumber",
          "input": true
        }
      ]
    }

    this.formioDateConfig  = {
      "components": [
        {
          "label": "Date Label",
          "format": "yyyy-MM-dd",
          "enableTime": false,
          "labelPosition": "top",
          "placeholder": "This is a date field",
          "suffix": "<i ref=\"icon\" class=\"fa fa-calendar\" style=\"\"></i>",
          "widget": {
            "type": "calendar",
            "format": "yyyy-MM-dd"
          },
          "validate": {
            "required": false
          },
          "key": "model-property-name",
          "type": "datetime",
          "input": true
        }
      ]
    }

    this.formioRadioConfig  = {
      "components": [
        {
          "label": "Radio buttons",
          "labelPosition": "top",
          "inline": true,
          "values": [
          {
              "label": "1",
              "value": "1"
            },
            {
              "label": "2",
              "value": "2"
            },
            {
              "label": "3",
              "value": "3"
            }
          ],
          "validate": {
            "required": false
          },
          "key": "model-property-name",
          "type": "radio",
          "input": true
        }
      ]
    }

    this.formioCheckboxConfig = {
      "components": [
        {
          "label": "Checkbox",
          "defaultValue": false,
          "key": "checkbox",
          "type": "checkbox",
          "input": true
        }
      ]
    }
    
    this.formioSelectHtml5Config = {
      "components": [
        {
          "label": "Dropdown list",
          "labelPosition": "top",
          "dataSrc": "url",
          "data": {
              "url": "APIendpoint",
              "headers": []
          },
          "template": "<span class='col-4 text-truncate'>{{ item.alternateName }}</span>",
          "selectValues": "value",
          "widget": "html5",
          "validate": {
              "required": false
          },
          "key": "model-property-name",
          "type": "select",
          "input": true,
        }
      ]
    }

    this.formioSelectChoicesJSConfig = {
      "components": [
        {
          "label": "Dropdown list",
          "labelPosition": "top",
          "dataSrc": "url",
          "data": {
              "url": "APIendpoint",
              "headers": []
          },
          "template": "<span class='col-4 text-truncate'>{{ item.alternateName }}</span>",
          "selectValues": "value",
          "widget": "choicesjs",
          "validate": {
              "required": false
          },
          "key": "model-property-name",
          "type": "select",
          "input": true,
        }
      ]
    }

    this.formioTooltipConfig = {
      "components": [
        {
          "label": "Informational tooltip for an element",
          "tooltip": "Check this if you need to check it",
          "defaultValue": false,
          "key": "checkbox",
          "type": "checkbox",
          "input": true
        }
      ]
    }

    this.formioDescriptionConfig = {
      "components": [
        {
          "label": "Description for an element",
          "labelPosition": "top",
          "placeholder": "This is a textfield",
          "description" : "This is the decription for the field above",
          "widget": {
            "type": "input"
          },
          "validate": {
            "required": false
          },
          "key": "model-property-name",
          "type": "textfield",
          "input": true
        }
      ]
    }

    this.formioLayoutColumnsConfig = {
      "components": [
          {
              "label": "Columns",
              "columns": [
                  {
                      "components": [
                        {
                          "label": "Column 1 Component",
                          "labelPosition": "top",
                          "placeholder": "Simple textfield",
                          "key": "model-property-name",
                          "type": "textfield"
                        }
                      ],
                      "width": 4
                  },
                  {
                      "components": [
                        {
                          "label": "Column 2 Component",
                          "labelPosition": "top",
                          "placeholder": "Simple textfield",
                          "key": "model-property-name",
                          "type": "textfield"
                        }
                      ],
                      "width": 4
                  },
                  {
                      "components": [
                        {
                          "label": "Column 3 Component",
                          "labelPosition": "top",
                          "placeholder": "Simple textfield",
                          "key": "model-property-name",
                          "type": "textfield"
                        }
                      ],
                      "width": 4
                  }
              ],
              "key": "columns",
              "type": "columns",
              "input": false
          }
        ]
      }

    this.formioLayoutFieldsetConfig = {
      "components": [
        {
          "legend": "Group 1",
          "type": "fieldset",
          "components": [
            {
              "label": "Fieldset 1 Component",
              "labelPosition": "top",
              "placeholder": "Simple textfield",
              "key": "model-property-name",
              "type": "textfield"
            }
          ]
        },
        {
          "legend": "Group 2",
          "type": "fieldset",
          "components": [
            {
              "label": "Fieldset 1 Component",
              "labelPosition": "top",
              "placeholder": "Simple textfield",
              "key": "model-property-name",
              "type": "textfield"
            }
          ]
        },
      ]
    }

    this.formioValidationSimpleConfig = {
      "components": [
        {
          "label": "A Textfield Label",
          "labelPosition": "top",
          "placeholder": "You should insert something in here",
          "widget": {
            "type": "input"
          },
          "validate": {
            "required": true
          },
          "key": "model-property-name",
          "type": "textfield",
          "input": true
        }
      ]
    }

    this.formioValidationNumberConfig = {
      "components": [
        {
          "label": "A Number Label",
          "labelPosition": "top",
          "placeholder": "If type is set to number it automatically expects a number to be entered",
          "widget": {
            "type": "input"
          },
          "validate": {
            "required": true
          },
          "key": "model-property-name",
          "type": "number",
          "input": true
        }
      ]
    }

    this.formioValidationMinMaxNumberConfig = {
      "components": [
        {
          "label": "A Number Label",
          "labelPosition": "top",
          "placeholder": "You are expected to insert an integer between 1 and 5",
          "widget": {
            "type": "input"
          },
          "validate": {
            "required": true,
            "integer": true,
            "min": 1,
            "max": 5
          },
          "key": "model-property-name",
          "type": "number",
          "input": true
        }
      ]
    }

    this.formioValidationMaskNumberConfig = {
      "components": [
        {
          "label": "A masked text field",
          "labelPosition": "top",
          "placeholder": "You are expected to insert a 4 digit number",
          "inputMask": "9999",
          "widget": {
            "type": "input"
          },
          "validate": {
            "required": true
          },
          "key": "model-property-name",
          "type": "textfield",
          "input": true
        }
      ]
    }

    this.formioValidationRegExpConfig = {
      "components": [
        {
          "label": "Validate using a regular expression",
          "labelPosition": "top",
          "placeholder": "Expected input is in the form 11-AAA-1111.AA",
          "widget": {
            "type": "input"
          },
          "validate": {
            "required": true,
            "pattern": "[0-9]{2}-[A-Z]{3}-[0-9]{4}\.[A-Z]{2}"
          },
          "key": "model-property-name",
          "type": "textfield",
          "input": true
        }
      ]
    }

    this.formioValidationCustomConfig = {
      "components": [
        {
          "label": "Custom Validation and error message",
          "labelPosition": "top",
          "placeholder": "Expected a 10 digit integer as a number",
          "widget": {
            "type": "input"
          },
          "validate": {
            "required": true,
            "integer": true,
            "custom": "valid = (/^[0-9]{10}$/.test(input)) ? true : 'Please enter a 10 digit number';"
          },
          "key": "model-property-name",
          "type": "number",
          "input": true
        }
      ]
    }

    this.formioValidateDisabledInput = {
      "components": [
        {
          "label": "Textfield disabled for input",
          "labelPosition": "top",
          "disabled": true,
          "placeholder": "This is a textfield",
          "validate": {
            "required": false
          },
          "key": "model-property-name",
          "type": "textfield",
          "input": false
        }
      ]
    }
    
    this.formioStyleCustomClassConfig = {
      "components": [
        {
          "label": "CSS styling using custom class",
          "labelPosition": "top",
          "customClass": "text-success",
          "placeholder": "This is a textfield",
          "widget": {
            "type": "input"
          },
          "validate": {
            "required": false
          },
          "key": "model-property-name",
          "type": "textfield",
          "input": true
        }
      ]
    }

    $('#formioTextfieldConfig').text(JSON.stringify(this.formioTextfieldConfig, null, 4));
    $('#formioEmailConfig').text(JSON.stringify(this.formioEmailConfig, null, 4));
    $('#formioNumberConfig').text(JSON.stringify(this.formioNumberConfig, null, 4));
    $('#formioPhoneNumberConfig').text(JSON.stringify(this.formioPhoneNumberConfig, null, 4));
    $('#formioDateConfig').text(JSON.stringify(this.formioDateConfig, null, 4));
    $('#formioRadioConfig').text(JSON.stringify(this.formioRadioConfig, null, 4));
    $('#formioCheckboxConfig').text(JSON.stringify(this.formioCheckboxConfig, null, 4));
    $('#formioSelectHtml5Config').text(JSON.stringify(this.formioSelectHtml5Config, null, 4));
    $('#formioSelectChoicesJSConfig').text(JSON.stringify(this.formioSelectChoicesJSConfig, null, 4));
    $('#formioTooltipConfig').text(JSON.stringify(this.formioTooltipConfig, null, 4));
    $('#formioDescriptionConfig').text(JSON.stringify(this.formioDescriptionConfig, null, 4));
    $('#formioLayoutColumnsConfig').text(JSON.stringify(this.formioLayoutColumnsConfig, null, 4));
    $('#formioLayoutFieldsetConfig').text(JSON.stringify(this.formioLayoutFieldsetConfig, null, 4));
    $('#formioValidationSimpleConfig').text(JSON.stringify(this.formioValidationSimpleConfig, null, 4));
    $('#formioValidationNumberConfig').text(JSON.stringify(this.formioValidationNumberConfig, null, 4));
    $('#formioValidationMinMaxNumberConfig').text(JSON.stringify(this.formioValidationMinMaxNumberConfig, null, 4));
    $('#formioValidationMaskNumberConfig').text(JSON.stringify(this.formioValidationMaskNumberConfig, null, 4));
    $('#formioValidationRegExpConfig').text(JSON.stringify(this.formioValidationRegExpConfig, null, 4));
    $('#formioValidationCustomConfig').text(JSON.stringify(this.formioValidationCustomConfig, null, 4));
    $('#formioValidateDisabledInput').text(JSON.stringify(this.formioValidateDisabledInput, null, 4));
    $('#formioStyleCustomClassConfig').text(JSON.stringify(this.formioStyleCustomClassConfig, null, 4));
  }
}


