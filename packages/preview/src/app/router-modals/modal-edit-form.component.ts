import { Component, ViewChild, OnInit, OnDestroy, EventEmitter } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {NgForm} from '@angular/forms';
import { Subscription } from 'rxjs';
import {RouterModalOkCancel} from '@universis/common/routing';
@Component({
    selector: 'app-modal-edit',
    templateUrl: './modal-edit-form.component.html',
    styles: [`
    `]
})
export class ModalEditComponent extends RouterModalOkCancel implements OnInit, OnDestroy {
    public modalTitle  = 'A modal window as route';
    public modalClass = 'modal-xl';
    public okButtonText = 'Continue';
    public statusChangesSubscription: Subscription;

    @ViewChild('form') form: NgForm;

    public model = {
        email: null,
        password: null,
        checkMeOut: true
    };
    //
    constructor(protected router: Router, protected activatedRoute: ActivatedRoute) {
        super(router, activatedRoute);
    }

    ngOnInit() {
        this.statusChangesSubscription = this.form.statusChanges.subscribe( status => {
            // set ok button status from form status
            this.okButtonDisabled =  this.form.invalid;
        });
    }

    ngOnDestroy() {
        if (this.statusChangesSubscription) {
            this.statusChangesSubscription.unsubscribe();
        }
    }

    cancel() {
        // close modal
        return this.close();
    }

    ok() {
        // do something (wait for 5 seconds)
        return new Promise( resolve => {
            setTimeout(() => {
                // close modal and return
                return this.close().then(() => {
                    return resolve();
                });
            }, 5000);
        });
    }
}
