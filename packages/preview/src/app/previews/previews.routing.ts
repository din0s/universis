import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {GraduationRulesComponent} from './components/graduation-rules/graduation-rules.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'graduation-rules'
  },
  {
    path: 'graduation-rules',
    component: GraduationRulesComponent,
    data: {
      title: 'Student Graduation Rules'
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PreviewsRoutingModule {
  constructor() {}
}
