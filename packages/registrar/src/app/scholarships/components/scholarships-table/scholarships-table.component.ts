import { Component, OnInit, ViewChild } from '@angular/core';
import * as SCHOLARSHIPS_LIST_CONFIG from './scholarships-table.config.json';
import { AdvancedTableComponent } from '../../../tables/components/advanced-table/advanced-table.component.js';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-scholarships-table',
  templateUrl: './scholarships-table.component.html',
  styleUrls: ['./scholarships-table.component.scss']
})
export class ScholarshipsTableComponent implements OnInit {

  public readonly config = SCHOLARSHIPS_LIST_CONFIG;

  @ViewChild('table') table: AdvancedTableComponent;

  onSearchKeyDown(event: any) {
    if (event.keyCode === 13) {
      this.table.search((<HTMLInputElement>event.target).value);
      return false;
    }
  }

  constructor(private _router: Router, private _activatedRoute: ActivatedRoute) { }

  ngOnInit() {
  }

}
