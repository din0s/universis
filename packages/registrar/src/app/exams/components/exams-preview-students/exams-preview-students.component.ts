import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdvancedTableComponent, AdvancedTableConfiguration } from '../../../tables/components/advanced-table/advanced-table.component';
import { AngularDataContext } from '@themost/angular';
import * as EXAMS_STUDENTS_LIST_CONFIG from './exams-students-table.config.json';

@Component({
  selector: 'app-exams-preview-students',
  templateUrl: './exams-preview-students.component.html',
  styleUrls: ['./exams-preview-students.component.scss']
})
export class ExamsPreviewStudentsComponent implements OnInit {

  public readonly config = EXAMS_STUDENTS_LIST_CONFIG;
  @ViewChild('students') students: AdvancedTableComponent;

  constructor(private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext
  ) { }

  async ngOnInit() {
    this.students.query = this._context.model(`CourseExams/${this._activatedRoute.snapshot.params.id}/students`)
      .asQueryable().prepare();
    this.students.config = AdvancedTableConfiguration.cast(EXAMS_STUDENTS_LIST_CONFIG);
    // declare model for advance search filter criteria
    this.students.config.model = 'CourseExams/' + this._activatedRoute.snapshot.params.id + '/students';
  }
}
