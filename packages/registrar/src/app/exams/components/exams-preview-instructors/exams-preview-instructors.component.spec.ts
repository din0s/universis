import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamsPreviewInstructorsComponent } from './exams-preview-instructors.component';

describe('ExamsPreviewInstructorsComponent', () => {
  let component: ExamsPreviewInstructorsComponent;
  let fixture: ComponentFixture<ExamsPreviewInstructorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExamsPreviewInstructorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamsPreviewInstructorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
