import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExamsHomeComponent } from './components/exams-home/exams-home.component';
import { ExamsRoutingModule } from './exams.routing';
import { ExamsSharedModule } from './exams.shared';
import { ExamsTableComponent } from './components/exams-table/exams-table.component';
import { ExamsPreviewComponent } from './components/exams-preview/exams-preview.component';
import { ExamsRootComponent } from './components/exams-root/exams-root.component';
import { TablesModule } from '../tables/tables.module';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '@universis/common';
import { ExamsPreviewGeneralComponent } from './components/exams-preview-general/exams-preview-general.component';
import { ExamsPreviewStudentsComponent } from './components/exams-preview-students/exams-preview-students.component';
import { ExamsPreviewInstructorsComponent } from './components/exams-preview-instructors/exams-preview-instructors.component';
import { StudentsSharedModule } from './../students/students.shared';
import { InstructorsSharedModule } from './../instructors/instructors.shared';
import { ElementsModule } from '../elements/elements.module';
import { ExamsPreviewGradingComponent } from './components/exams-preview-grading/exams-preview-grading.component';
import {UploadsPreviewFormComponent} from './components/exams-preview-grading/uploads-preview-form.component';
import { ExamsPreviewGradesCheckComponent } from './components/exams-preview-grades-check/exams-preview-grades-check.component';
import {ExamsAdvancedTableSearchComponent} from './components/exams-table/exams-advanced-table-search.component';
import {ExamsStudentsAdvancedTableSearchComponent} from './components/exams-preview-students/exams-students-advanced-table-search.component';
import {MostModule} from '@themost/angular';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { DatePipe } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    ExamsSharedModule,
    TablesModule,
    ExamsRoutingModule,
    FormsModule,
    SharedModule,
    StudentsSharedModule,
    InstructorsSharedModule,
    ElementsModule,
    MostModule,
    BsDatepickerModule.forRoot()
  ],
  providers: [DatePipe],
  declarations: [
    ExamsHomeComponent,
    ExamsTableComponent,
    ExamsPreviewComponent,
    ExamsRootComponent,
    ExamsPreviewGeneralComponent,
    ExamsPreviewStudentsComponent,
    ExamsPreviewInstructorsComponent,
    ExamsPreviewGradingComponent,
    UploadsPreviewFormComponent,
    ExamsPreviewGradesCheckComponent,
    ExamsAdvancedTableSearchComponent,
    ExamsStudentsAdvancedTableSearchComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ExamsModule { }
