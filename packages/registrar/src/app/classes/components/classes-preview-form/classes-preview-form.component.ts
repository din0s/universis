import {Component, Input, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';

@Component({
  selector: 'app-classes-preview-form',
  templateUrl: './classes-preview-form.component.html',
  styleUrls: ['./classes-preview-form.component.scss']
})
export class ClassesPreviewFormComponent implements OnInit {

  @Input() model: any;

  constructor(private _context: AngularDataContext) { }

  ngOnInit() {
  }

}
