import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassesPreviewComponent } from './classes-preview.component';

describe('ClassesPreviewComponent', () => {
  let component: ClassesPreviewComponent;
  let fixture: ComponentFixture<ClassesPreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassesPreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassesPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
