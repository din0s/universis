import {Component, OnInit, ViewChild} from '@angular/core';
import * as CLASSES_LIST_CONFIG from './classes-table.config.json';
import {AdvancedTableComponent} from '../../../tables/components/advanced-table/advanced-table.component';
import {ActivatedTableService} from '../../../tables/tables.activated-table.service';

@Component({
  selector: 'app-classes-table',
  templateUrl: './classes-table.component.html',
  styles: []
})
export class ClassesTableComponent implements OnInit {

  @ViewChild('table') table: AdvancedTableComponent;

  public readonly config = CLASSES_LIST_CONFIG;

  constructor(private _activatedTable: ActivatedTableService) { }

  ngOnInit() {
    this._activatedTable.activeTable = this.table ;
  }

}
