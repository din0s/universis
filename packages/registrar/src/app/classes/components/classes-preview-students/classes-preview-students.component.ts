import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdvancedTableComponent, AdvancedTableConfiguration } from '../../../tables/components/advanced-table/advanced-table.component';
import { AngularDataContext } from '@themost/angular';
import * as CLASSES_STUDENTS_LIST_CONFIG from './classes-preview-students.config.json';

@Component({
  selector: 'app-classes-preview-students',
  templateUrl: './classes-preview-students.component.html'
})
export class ClassesPreviewStudentsComponent implements OnInit {

  public readonly config = CLASSES_STUDENTS_LIST_CONFIG;
  @ViewChild('students') students: AdvancedTableComponent;

  constructor(private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext) { }

  async ngOnInit() {
    this.students.query = this._context.model('CourseClasses/' + this._activatedRoute.snapshot.params.id + '/students')
      .asQueryable()
      .expand('student($expand=department,person,studyProgram,semester,studentStatus,inscriptionYear,inscriptionPeriod)')
      .prepare();
    this.students.config = AdvancedTableConfiguration.cast(CLASSES_STUDENTS_LIST_CONFIG);
    // declare model for advance search filter criteria
    this.students.config.model = 'CourseClasses/' + this._activatedRoute.snapshot.params.id + '/students';
  }
}
