import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AdvancedTableComponent, AdvancedTableConfiguration} from '../../../../tables/components/advanced-table/advanced-table.component';
import {AngularDataContext} from '@themost/angular';
import * as STUDYPROGRAMS_COURSES_LIST_CONFIG from './study-programs-preview-courses.config.json';

@Component({
  selector: 'app-study-programs-preview-courses',
  templateUrl: './study-programs-preview-courses.component.html'
})
export class StudyProgramsPreviewCoursesComponent implements OnInit {


  public readonly config = STUDYPROGRAMS_COURSES_LIST_CONFIG;
  @ViewChild('courses') courses: AdvancedTableComponent;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext
  ) { }

  async ngOnInit() {
    this.courses.query =  this._context.model('ProgramCourses')
      .where('program').equal(this._activatedRoute.snapshot.params.id)
      .expand('course')
      .prepare();
    this.courses.config = AdvancedTableConfiguration.cast(STUDYPROGRAMS_COURSES_LIST_CONFIG);
  }
}
