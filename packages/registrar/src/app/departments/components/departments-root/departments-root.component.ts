import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-departments-root',
  template: `
  <div class="row">
  <div class="col-lg-8 col-md-8 tab-navbar tab-container">
    <a class="text-decoration-none text-secondary font-lg" [routerLink]="['../']">
      <i class="fa fa-chevron-left"></i> {{ 'Sidebar.Departments' | translate }}
    </a>
  </div>
</div>
  <router-outlet></router-outlet>`,
})
export class DepartmentsRootComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    //
  }

}
