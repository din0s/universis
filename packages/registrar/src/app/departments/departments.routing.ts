import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DepartmentsHomeComponent } from './components/departments-home/departments-home.component';
import { DepartmentsTableComponent } from './components/departments-table/departments-table.component';
import { DepartmentsRootComponent } from './components/departments-root/departments-root.component';
import { DepartmentsPreviewComponent } from './components/departments-preview/departments-preview.component';
import { DepartmentsPreviewGeneralComponent } from './components/departments-preview/departments-preview-general/departments-preview-general.component';
import {AdvancedFormRouterComponent} from '../registrar-shared/advanced-form-router/advanced-form-router.component';

const routes: Routes = [
    {
        path: '',
        component: DepartmentsHomeComponent,
        data: {
            title: 'Departments'
        },
        children: [
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'list'
            },
            {
                path: 'list',
                component: DepartmentsTableComponent,
                data: {
                    title: 'Departments List'
                }
            },
            {
                path: 'active',
                component: DepartmentsTableComponent,
                data: {
                    title: 'Active Departments'
                }
            }
        ]
    },
    {
        path: ':id',
        component: DepartmentsRootComponent,
        data: {
            title: 'Departments Home'
        },
        children: [
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'preview'
            },
            {
                path: 'preview',
                component: DepartmentsPreviewComponent,
                data: {
                    title: 'Departments Preview'
                },
                children: [
                    {
                        path: '',
                        redirectTo: 'general'
                    },
                    {
                        path: 'general',
                        component: DepartmentsPreviewGeneralComponent,
                        data: {
                            title: 'Department\'s View'
                        }
                    }
                ]

            },
            {
                path: ':action',
                component: AdvancedFormRouterComponent
            }
        ]
  }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: []
})
export class DepartmentsRoutingModule {
}
