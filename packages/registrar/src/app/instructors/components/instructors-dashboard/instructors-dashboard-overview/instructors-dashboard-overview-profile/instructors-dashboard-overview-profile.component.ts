import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';

@Component({
  selector: 'app-instructors-dashboard-overview-profile',
  templateUrl: './instructors-dashboard-overview-profile.component.html',
  styleUrls: ['./instructors-dashboard-overview-profile.component.scss']
})
export class InstructorsDashboardOverviewProfileComponent implements OnInit {
  public model: any;
  @Input() id: any;

  constructor(public _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) { }

  async ngOnInit() {
    this.model = await this._context.model('Instructors')
      .where('id').equal(this._activatedRoute.snapshot.params.id)
      .expand('user,department($expand=organization)')
      .getItem();
  }

}
