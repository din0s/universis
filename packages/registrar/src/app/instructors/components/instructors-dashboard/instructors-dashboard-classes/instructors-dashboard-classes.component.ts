import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AdvancedTableComponent, AdvancedTableConfiguration} from '../../../../tables/components/advanced-table/advanced-table.component';
import {AngularDataContext} from '@themost/angular';
import * as INSTRUCTORS_CLASSES_LIST_CONFIG from './instructors-dashboard-classes.config.json';
import { ActivatedTableService } from '../../../../tables/tables.activated-table.service.js';

@Component({
  selector: 'app-instructors-dashboard-classes',
  templateUrl: './instructors-dashboard-classes.component.html',
  styleUrls: ['../instructors-dashboard.component.scss']
})
export class InstructorsDashboardClassesComponent implements OnInit {

  public readonly config = INSTRUCTORS_CLASSES_LIST_CONFIG;
  @ViewChild('classes') classes: AdvancedTableComponent;
  instructorID: any = this._activatedRoute.snapshot.params.id;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _activatedTable: ActivatedTableService
  ) { }

  async ngOnInit() {
    this._activatedTable.activeTable = this.classes;
    this.classes.query =  this._context.model('courseClassInstructors')
      .where('instructor').equal(this._activatedRoute.snapshot.params.id)
      .expand('courseClass($expand=course,year,period)')
      .prepare();
    this.classes.config = AdvancedTableConfiguration.cast(INSTRUCTORS_CLASSES_LIST_CONFIG);
  }

}
