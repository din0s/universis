import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import * as INSTRUCTORS_LIST_CONFIG from './instructors-table.config.json';
import {AdvancedTableComponent} from '../../../tables/components/advanced-table/advanced-table.component';
import {ActivatedRoute, Router} from '@angular/router';
import {ActivatedTableService} from '../../../tables/tables.activated-table.service';

@Component({
  selector: 'app-instructors-table',
  templateUrl: './instructors-table.component.html'
})
export class InstructorsTableComponent implements OnInit {

  public readonly config = INSTRUCTORS_LIST_CONFIG;

  @ViewChild('table') table: AdvancedTableComponent;

  constructor(private _router: Router, private _activatedRoute: ActivatedRoute, private _activatedTable: ActivatedTableService) { }

  ngOnInit() {
    this._activatedTable.activeTable = this.table ;
  }

}
