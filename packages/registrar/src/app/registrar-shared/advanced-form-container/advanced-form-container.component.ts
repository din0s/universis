import {Component, Input, OnInit, ViewChild} from '@angular/core';
import { AdvancedFormComponent } from 'packages/forms/src/public_api';

@Component({
  selector: 'app-advanced-form-container',
  templateUrl: './advanced-form-container.component.html'
})
export class AdvancedFormContainerComponent implements OnInit {

  @Input() data: any;
  @Input() src: any;
  @ViewChild('form') form: AdvancedFormComponent;

  constructor() { }

  ngOnInit() {
    //
  }

}
