import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvancedFormContainerComponent } from './advanced-form-container.component';

describe('AdvancedFormContainerComponent', () => {
  let component: AdvancedFormContainerComponent;
  let fixture: ComponentFixture<AdvancedFormContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvancedFormContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvancedFormContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
