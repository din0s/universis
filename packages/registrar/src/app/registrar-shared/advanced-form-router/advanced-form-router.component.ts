import {Component, OnDestroy, OnInit} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {combineLatest, Observable, Subscription} from 'rxjs';
import { AdvancedFormContainerComponent } from '../advanced-form-container/advanced-form-container.component';
import { AngularDataContext } from '@themost/angular';
import { ErrorService } from '@universis/common';
import { ActiveDepartmentService } from '../services/activeDepartmentService.service';
import {EdmSchema} from '@themost/client';

@Component({
  selector: 'app-advanced-form-router',
  templateUrl: '../advanced-form-container/advanced-form-container.component.html',
  styles: []
})
export class AdvancedFormRouterComponent extends AdvancedFormContainerComponent implements OnInit, OnDestroy {

  private subscription: Subscription;
  private schema: EdmSchema;

  constructor(private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext, private _activeDepartment: ActiveDepartmentService,
    private _errorService: ErrorService) {
    super();
  }

  ngOnInit() {
    this.subscription = combineLatest(
        this._activatedRoute.params,
        this._activatedRoute.data,
        new Observable(subscriber => {
          this._context.getMetadata().then( schema => {
            return subscriber.next(schema);
          }).catch( err => {
            subscriber.error(err);
          });
        })
    ).subscribe((results: any) => {
      // get data
      const routeData = results[1];
      // get params
      const params = Object.assign({}, results[0], results[1]);
      // get schema
      this.schema = results[2];
      // if params has a model defined e.g. LocalDepartments
      // and an action e.g. edit
      if (params.model && params.action) {
        // set form src
        this.src = `${params.model}/${params.action}`;
        if (params.id) {
          // try to get model data
          this._context.model(params.model).asQueryable(params.serviceParams).prepare()
            .where('id').equal(params.id).getItem().then(item => {
              this.data = item;
            }).catch(err => {
              this._errorService.showError(err);
            });
        }
        // validate new form state by using routeConfig.path === 'new'
        // todo:: change the way that identifies whether a form refers to a new object or not
      } else if (params.model && this._activatedRoute && this._activatedRoute.snapshot
        && this._activatedRoute.snapshot.routeConfig && this._activatedRoute.snapshot.routeConfig.path
        && this._activatedRoute.snapshot.routeConfig.path === 'new') {
        // assign route data to model
        if (routeData) {
          const data = {};
          // pass only data that are properties or navigation properties of the specified model
          this._assign(params.model, data, routeData);
          // set form src
          // todo:: change the assignment of form source with something more configurable
          this.src = `${params.model}/new`;
          // set data
          this.data = data;
        }
      }
    });
  }

  _assign(entitySet, dest, source) {
    const add = { };
    // get metadata
    const schema =  this.schema;
    // find entity set based on model name
    const findEntitySet = schema.EntityContainer.EntitySet.find( x => {
      return x.Name === entitySet;
    });
    if (findEntitySet) {
      // find entity type
      const findEntityType = schema.EntityType.find( x => {
        return x.Name === findEntitySet.EntityType;
      });
      if (findEntityType) {
        // enumerate params
        const  keys = Object.keys(source);
        for (let i = 0; i < keys.length; i++) {
          const key = keys[i];
          if (Object.prototype.hasOwnProperty.call(source, key)) {
            const property = key.split('/');
            if (property.length === 1) {
              const testProperty = new RegExp(`^${property[0]}$`);
              // check if query param is a property of the specified model
              const findProperty = findEntityType.Property.find( x => {
                return testProperty.test(x.Name);
              });
              // and assign value
              if (findProperty) {
                Object.defineProperty(add, findProperty.Name, {
                  enumerable: true,
                  configurable: true,
                  writable: true,
                  value: source[key]
                });
              } else {
                // search navigation property
                const findNavigationProperty = findEntityType.NavigationProperty.find( x => {
                  return testProperty.test(x.Name);
                });
                // if query param is a navigation property
                if (findNavigationProperty) {
                  // and set property
                  Object.defineProperty(add, findNavigationProperty.Name, {
                    enumerable: true,
                    configurable: true,
                    writable: true,
                    value: source[key]
                  });
                }
              }
            }
          }
        }
      }
      // finally assign values
      Object.assign(dest, add);
      return dest;
    }
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}
