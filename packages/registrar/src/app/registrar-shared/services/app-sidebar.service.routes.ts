export const APP_SIDEBAR_ITEMS = [
    {
        "name": "Sidebar.Instructors",
        "url": "/instructors",
        "icon": "fa fa-bullhorn",
        "index": 0
    },
    {
        "name": "Sidebar.Students",
        "url": "/students",
        "icon": "fa fa-graduation-cap",
        "index": 10
    },
    {
        "name": "Sidebar.Teaching",
        "url":"javascript:void(0)",
        "icon": "fa fa-history",
        "index": 20,
        "children": [
            {
                "name": "Sidebar.Courses",
                "url": "/courses",
                "icon": "fa fa-none",
                "index": 0
            },
            {
                "name": "Sidebar.Classes",
                "url": "/classes",
                "icon": "fa fa-none",
                "index": 5
            },
            {
                "name": "Sidebar.Exams",
                "url": "/exams",
                "icon": "fa fa-none",
                "index": 10
            }
        ]
    },
    {
        "name": "Sidebar.StudyPrograms",
        "url": "/study-programs",
        "icon": "fa fa-university",
        "index": 30
    },
    {
        "name": "Sidebar.Theses",
        "url": "/theses",
        "icon": "fa fa-briefcase",
        "index": 40
    },
    {
        "name": "Sidebar.Requests",
        "url": "/requests",
        "icon": "fa fa-envelope",
        "index": 50
    },
    {
        "name": "Sidebar.Registrations",
        "url": "/registrations",
        "icon": "fa fa-clipboard",
        "index": 60
    },
    {
        "name": "Sidebar.Users",
        "url": "/users",
        "icon": "fa fa-user",
        "index": 60
    },
    {
        "name": "Sidebar.More",
        "url":"javascript:void(0)",
        "class": "open",
        "icon": "fa fa-archive",
        "index": 70,
        "children": [
            {
                "name": "Sidebar.Graduation",
                "url": "/students/graduated",
                "icon": "fa fa-none",
                "index": 0
            },
            {
                "name": "Sidebar.Scholarships",
                "url": "/scholarships",
                "icon": "fa fa-none",
                "index": 5
            },
            {
                "name": "Sidebar.Internships",
                "url": "/internships",
                "icon": "fa fa-none",
                "index": 10
            },
            {
                "name": "Sidebar.Statistics",
                "url": "/courses/exams",
                "icon": "fa fa-none",
                "index": 20
            },
            {
                "name": "Sidebar.Departments",
                "url": "/departments",
                "icon": "fa fa-none",
                "index": 25
            },
            {
                "name": "Sidebar.RequestDocuments",
                "url": "/requests/documents",
                "icon": "fa fa-none",
                "index": 30
            }
        ]
    }
];
