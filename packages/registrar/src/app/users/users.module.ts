import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersRootComponent } from './components/users-root/users-root.component';
import { UsersHomeComponent } from './components/users-home/users-home.component';
import { UsersTableComponent } from './components/users-table/users-table.component';
import { UsersSharedModule } from './users.shared';
import { UsersRoutingModule } from './users.routing';
import { TablesModule } from '../tables/tables.module';
import { TranslateModule } from '@ngx-translate/core';
import {UsersAdvancedTableSearchComponent} from './components/users-table/users-advanced-table-search.component';
import {SharedModule} from '@universis/common';
import {MostModule} from '@themost/angular';
import {FormsModule} from '@angular/forms';
import {ElementsModule} from '../elements/elements.module';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    TablesModule,
    UsersRoutingModule,
    UsersSharedModule,
    SharedModule,
    MostModule,
    FormsModule,
    ElementsModule
  ],
  declarations: [UsersRootComponent,
    UsersHomeComponent,
    UsersTableComponent,
    UsersAdvancedTableSearchComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class UsersModule { }
