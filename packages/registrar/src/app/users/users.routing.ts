import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UsersRootComponent } from './components/users-root/users-root.component';
import {UsersHomeComponent} from './components/users-home/users-home.component';
import {UsersTableComponent} from './components/users-table/users-table.component';


const routes: Routes = [
    {
        path: '',
        component: UsersHomeComponent,
        data: {
            title: 'Users Home'
        },
        children: [
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'list'
            },
            {
                path: 'list',
                component: UsersTableComponent,
                data: {
                    title: 'Users List'
                }
            }
        ]
    },
    {
        path: ':id',
        component: UsersRootComponent,
        data: {
            title: 'User Home'
        },
        children: [
            {
                path: '',
                pathMatch: 'full',
                redirectTo: ''
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: []
})
export class UsersRoutingModule {
}
