import { Component, Input, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { DatePipe } from '@angular/common';

@Component({
    selector: 'app-users-advanced-table-search',
    templateUrl: './users-advanced-table-search.component.html',
    encapsulation: ViewEncapsulation.None
})

export class UsersAdvancedTableSearchComponent {

    @Input() filter: any;

    constructor(_context: AngularDataContext, _activatedRoute: ActivatedRoute, datePipe: DatePipe) {
        //
    }
}
