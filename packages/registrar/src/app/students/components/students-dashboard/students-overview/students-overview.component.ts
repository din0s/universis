import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-students-overview',
  templateUrl: './students-overview.component.html',
  styleUrls: ['./students-overview.component.scss']
})
export class StudentsOverviewComponent implements OnInit {

  public studentId;
  public showMore = true;

  constructor(private _activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.studentId = this._activatedRoute.snapshot.params.id;
  }

}
