import {Component, Input, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';

@Component({
  selector: 'app-students-overview-lastregistration',
  templateUrl: './students-overview-last-registration.component.html',
  styleUrls: ['./students-overview-last-registration.component.scss']
})
export class StudentsOverviewLastRegistrationComponent implements OnInit {
  public lastRegistration: any;
  @Input() studentId: number;

  constructor(private _context: AngularDataContext) { }

  async ngOnInit() {
    this.lastRegistration = await this._context.model('Students/' + this.studentId + '/LastPeriodRegistration')
      .asQueryable()
      .expand('documents($orderby=dateCreated desc;$expand=documentStatus),classes($select=registration,sum(ects) as ects,count(id) as total;$groupby=registration)')
      .getItem();
  }

}
