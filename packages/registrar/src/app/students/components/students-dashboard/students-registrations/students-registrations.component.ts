import {Component, ElementRef, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {LoadingService} from '@universis/common';

@Component({
  selector: 'app-students-registrations',
  templateUrl: './students-registrations.component.html'
})
export class StudentsRegistrationsComponent implements OnInit {
  public model: any;

  constructor(private _element: ElementRef,
              private _activatedRoute: ActivatedRoute,
              private _translate: TranslateService,
              private _context: AngularDataContext,
              private _loadingService: LoadingService) {
  }

  async ngOnInit() {
    this._loadingService.showLoading();
    this.model = await this._context.model('Students/' + this._activatedRoute.snapshot.params.id + '/registrations')
      .asQueryable()
      .expand('classes($expand=course,courseType,courseClass($expand=course($expand=instructor)),examPeriod)')
      .orderByDescending('registrationYear')
      .thenByDescending('registrationPeriod')
      .getItems();
    this._loadingService.hideLoading();

  }

}
