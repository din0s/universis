import {Component, Input, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';

@Component({
  selector: 'app-students-registration-classes-form',
  templateUrl: './registration-classes-form.component.html'
})
export class RegistrationClassesFormComponent implements OnInit {

  @Input() registration: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) {
  }

  async ngOnInit() {

  }

}
