import {CUSTOM_ELEMENTS_SCHEMA, NgModule, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {environment} from '../../environments/environment';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {StudentsOverviewCoursesComponent} from './components/students-dashboard/students-overview/students-overview-courses/students-overview-courses.component';
import {StudentsOverviewLastRegistrationComponent} from './components/students-dashboard/students-overview/students-overview-lastregistration/students-overview-last-registration.component';
import {StudentsOverviewCurrentgradesComponent} from './components/students-dashboard/students-overview/students-overview-currentgrades/students-overview-currentgrades.component';
import {StudentsOverviewRequestsComponent} from './components/students-dashboard/students-overview/students-overview-requests/students-overview-requests.component';
import {StudentsOverviewInternshipsComponent} from './components/students-dashboard/students-overview/students-overview-internships/students-overview-internships.component';
import {TooltipModule} from 'ngx-bootstrap';
import {StudentsOverviewScholarshipsComponent} from './components/students-dashboard/students-overview/students-overview-scholarships/students-overview-scholarships.component';
import {StudentsOverviewThesesComponent} from './components/students-dashboard/students-overview/students-overview-theses/students-overview-theses.component';
import { StudentsOverviewProfileComponent } from './components/students-dashboard/students-overview/students-overview-profile/students-overview-profile.component';
import {FormsModule} from '@angular/forms';
import {StudentsPreviewFormComponent} from './components/students-dashboard/students-general/students-preview-form.component';
import { StudentsMessagesPreviewFormComponent } from './components/students-dashboard/students-messages-preview-form/students-messages-preview-form.component';
import { StudentsStatsComponent } from './components/students-dashboard/students-courses/students-stats/students-stats.component';
import { MessagesSharedModule } from '../messages/messages.shared';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    TooltipModule.forRoot(),
    FormsModule,
    MessagesSharedModule
  ],
  declarations: [
    StudentsOverviewCoursesComponent,
    StudentsOverviewLastRegistrationComponent,
    StudentsOverviewCurrentgradesComponent,
    StudentsOverviewRequestsComponent,
    StudentsOverviewInternshipsComponent,
    StudentsOverviewScholarshipsComponent,
    StudentsOverviewThesesComponent,
    StudentsOverviewProfileComponent,
    StudentsPreviewFormComponent,
    StudentsMessagesPreviewFormComponent,
    StudentsStatsComponent    
  ],
  exports: [
    StudentsOverviewCoursesComponent,
    StudentsOverviewLastRegistrationComponent,
    StudentsOverviewCurrentgradesComponent,
    StudentsOverviewRequestsComponent,
    StudentsOverviewInternshipsComponent,
    StudentsOverviewScholarshipsComponent,
    StudentsOverviewThesesComponent,
    StudentsOverviewProfileComponent,
    StudentsPreviewFormComponent,
    StudentsMessagesPreviewFormComponent,
    StudentsStatsComponent
  ],
  providers: [
    StudentsOverviewCoursesComponent,
    StudentsOverviewLastRegistrationComponent,
    StudentsOverviewCurrentgradesComponent,
    StudentsOverviewRequestsComponent,
    StudentsOverviewInternshipsComponent,
    StudentsOverviewScholarshipsComponent,
    StudentsOverviewThesesComponent,
    StudentsStatsComponent,
    StudentsOverviewProfileComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class StudentsSharedModule implements OnInit {

  constructor(private _translateService: TranslateService) {
    this.ngOnInit().catch(err => {
      console.error('An error occurred while loading students shared module');
      console.error(err);
    });
  }

  async ngOnInit() {
    environment.languages.forEach( language => {
      import(`./i18n/students.${language}.json`).then((translations) => {
        this._translateService.setTranslation(language, translations, true);
      });
    });
  }

}
