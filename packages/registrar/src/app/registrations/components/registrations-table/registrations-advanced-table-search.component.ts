import { Component, Input, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { DatePipe } from '@angular/common';
import { AdvancedTableSearchBaseComponent } from '../../../tables/components/advanced-table/advanced-table-search-base';

@Component({
    selector: 'app-registrations-advanced-table-search',
    templateUrl: './registrations-advanced-table-search.component.html',
    encapsulation: ViewEncapsulation.None
})

export class RegistrationsAdvancedTableSearchComponent extends AdvancedTableSearchBaseComponent {

    constructor(_context: AngularDataContext, _activatedRoute: ActivatedRoute, datePipe: DatePipe) {
        super();
    }
}
