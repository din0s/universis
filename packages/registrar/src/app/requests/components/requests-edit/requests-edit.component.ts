import {Component, OnInit, ViewChild} from '@angular/core';
import {RequestsService} from '../../services/requests.service';
import {ActivatedRoute} from '@angular/router';
import {DIALOG_BUTTONS, ErrorService, LoadingService, ModalService} from '@universis/common';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-requests-edit',
  templateUrl: './requests-edit.component.html',
  styleUrls: ['./requests-edit.component.scss']
})
export class RequestsEditComponent implements OnInit {

  public request: any;
  public showResponseForm = false;
  public showCancelButton = false;
  public showNewResponseButton = false;
  public requestid;
  public statusses;
  public selectedStatus;

  @ViewChild('fileinput') fileinput;
  @ViewChild('statusBtn') statusBtn;

  public responseModel = {
    body: null,
    attachment: null
  };

  constructor(private _requestsService: RequestsService,
              private _route: ActivatedRoute,
              private _errorService: ErrorService,
              private _loadingService: LoadingService,
              private _modalService: ModalService,
              private _translate: TranslateService) { }

  ngOnInit() {
    this._requestsService.getActionStatusses().then((statusses) => {
      this.statusses = statusses;
      this.statusses.find(item => item.alternateName === 'CompletedActionStatus').viewname = 'accept';
      this.statusses.find(item => item.alternateName === 'CancelledActionStatus').viewname = 'reject';
      if (this.statusses.length > 0) {
        this.selectedStatus = this.statusses[0];
      }

      this._route.parent.params.subscribe(routeParams => {
        this.requestid = routeParams.id;
        this.loadData(this.requestid);
      });
    });

  }

  // loads general data
  loadData(requestid) {
    // initialize values
    this.responseModel.attachment = null;
    this.responseModel.body = null;

    this._loadingService.showLoading();
    this._requestsService.getRequestById(requestid).then((res) => {

      // when not found request stop process
      if (res === undefined) {
        this._loadingService.hideLoading();
        return ;
      }

      // set selected status the current status of request
      this.selectedStatus = this.statusses.find(item => item.alternateName === 'CompletedActionStatus');

      // choice type of request
      if (res.additionalType === 'RequestDocumentAction') {
        this._requestsService.getRequestDocumentById(requestid).then((request) => {
          this.request = request;
          this.checkStatus();
        });
      } else if (res.additionalType === 'RequestMessageAction') {
        this._requestsService.getRequestMessageById(requestid).then((request) => {
          this.request = request;
          this.checkStatus();
        });
      }
      this._loadingService.hideLoading();
    }, (err) => {
      this._loadingService.hideLoading();
      return this._errorService.navigateToError(err);
    });
  }

  // change ui elements according to status state
  checkStatus() {
    // checks if request is cancel
    if (this.request.actionStatus.alternateName === 'CancelledActionStatus') {
      this.showResponseForm = false;
      this.showCancelButton = false;
      this.showNewResponseButton = false;
    } else {

      // if there are messages , the form and cancel button will be hidden
      if (this.request.messages.length > 0) {
        this.showResponseForm = false;
        this.showCancelButton = false;
        this.showNewResponseButton = true;
      } else {
        // else the new response button will be hidden
        this.showResponseForm = true;
        this.showCancelButton = true;
        this.showNewResponseButton = false;
      }
    }
  }

  // sends a response and changes request status to completed
  save() {
    if (this.requestid) {
      if (this.selectedStatus.alternateName === 'CancelledActionStatus') {
        this._modalService.showDialog(this._translate.instant('Requests.Edit.ModalTitle'),
          this._translate.instant('Requests.Edit.ModalMessage'),
          DIALOG_BUTTONS.OkCancel
        ).then(res => {
          if (res === 'ok') {
            this.changeRequestStatus();
          }
        });
      } else {
        this.changeRequestStatus();
      }
    }
  }

  changeRequestStatus() {
    this._loadingService.showLoading();
    this._requestsService.sendResponse(this.request.student.id,
      this.requestid,
      this.responseModel).subscribe(res => {
      // complete the action
      this._requestsService.changeRequestStatus(this.requestid, this.selectedStatus).then((resAction) => {
        this.request.actionStatus.alternateName = this.selectedStatus.alternateName;
        this.showResponseForm = false;

        // reloads requests
        this.loadData(this.requestid);
      }, (err) => {
        this._loadingService.hideLoading();
        return this._errorService.navigateToError(err);
      });

    }, (err) => {
      this._loadingService.hideLoading();
      return this._errorService.navigateToError(err);
    });
  }

  // download the document file
  downloadFile(attachments) {
    this._requestsService.downloadFile(attachments);
  }

  // get the selected file
  onFileChanged(event) {
    this.responseModel.attachment = this.fileinput.nativeElement.files[0];
  }

  // reset the file of html input element
  reset() {
    this.fileinput.nativeElement.value = '';
    this.responseModel.attachment = null;
  }

  // onchange status
  onChangeStatus(status) {
    this.selectedStatus = status;
  }
}
