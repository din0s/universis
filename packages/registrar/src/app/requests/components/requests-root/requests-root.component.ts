import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-requests-root',
  template: '<router-outlet></router-outlet>',
})
export class RequestsRootComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
