import {Component, Input, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';

@Component({
  selector: 'app-internships-preview-general',
  templateUrl: './internships-preview-general.component.html'
})
export class InternshipsPreviewGeneralComponent implements OnInit {

  @Input() model: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) {
  }

  async ngOnInit() {
      this.model = await this._context.model('Internships')
          .where('id').equal(this._activatedRoute.snapshot.params.id)
          .expand('status,internshipPeriod,department,student($expand=person,studyProgram,user,department,inscriptionYear,studentStatus)')
          .getItem();
  }

}
