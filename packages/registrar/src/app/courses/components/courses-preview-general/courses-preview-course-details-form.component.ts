import {Component, Input, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';

@Component({
  selector: 'app-courses-course-details-form',
  templateUrl: './courses-preview-course-details-form.component.html'
})
export class CoursesPreviewCourseDetailsFormComponent implements OnInit {

  @Input() model: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) {
  }

  async ngOnInit() {

  }

}
