import { CUSTOM_ELEMENTS_SCHEMA, NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoursesSharedModule } from './courses.shared';
import { CoursesRoutingModule } from './courses.routing';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { CoursesHomeComponent } from './components/courses-home/courses-home.component';
import { CoursesTableComponent } from './components/courses-table/courses-table.component';
import { AppSidebarService } from '../registrar-shared/services/app-sidebar.service';
import { TablesModule } from '../tables/tables.module';
import { CoursesRootComponent } from './components/courses-root/courses-root.component';
import { CoursesPreviewGeneralComponent } from './components/courses-preview-general/courses-preview-general.component';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '@universis/common';
import { ClasseSharedModule } from '../classes/classes.shared';
import { CoursesPreviewClassesComponent } from './components/courses-preview-classes/courses-preview-classes.component';
import { ElementsModule } from '../elements/elements.module';
import { CoursesPreviewCourseDetailsFormComponent } from './components/courses-preview-general/courses-preview-course-details-form.component';
import { CoursesOverviewComponent } from './components/courses-overview/courses-overview.component';
import { CoursesExamsComponent } from './components/courses-exams/courses-exams.component';
import { StudyProgramsSharedModule } from '../study-programs/study-programs.shared';
import { CoursesStudyProgramsComponent } from './components/courses-study-programs/courses-study-programs.component';
import { ExamsSharedModule } from '../exams/exams.shared';
import { CoursesAdvancedTableSearchComponent } from './components/courses-table/courses-advanced-table-search.component';
import { CoursesClassesAdvancedTableSearchComponent } from './components/courses-preview-classes/courses-classes-advanced-table-search.component';
import {MostModule} from '@themost/angular';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    CoursesSharedModule,
    CoursesRoutingModule,
    ClasseSharedModule,
    StudyProgramsSharedModule,
    ExamsSharedModule,
    TablesModule,
    SharedModule,
    FormsModule,
    ElementsModule,
    MostModule
  ],
  declarations: [CoursesHomeComponent,
    CoursesTableComponent,
    CoursesRootComponent,
    CoursesPreviewGeneralComponent,
    CoursesPreviewClassesComponent,
    CoursesPreviewCourseDetailsFormComponent,
    CoursesOverviewComponent,
    CoursesExamsComponent,
    CoursesStudyProgramsComponent,
    CoursesAdvancedTableSearchComponent,
    CoursesClassesAdvancedTableSearchComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CoursesModule { }
