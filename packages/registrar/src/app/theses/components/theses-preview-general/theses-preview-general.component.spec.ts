import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThesesPreviewGeneralComponent } from './theses-preview-general.component';

describe('ThesesPreviewGeneralComponent', () => {
  let component: ThesesPreviewGeneralComponent;
  let fixture: ComponentFixture<ThesesPreviewGeneralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThesesPreviewGeneralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThesesPreviewGeneralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
