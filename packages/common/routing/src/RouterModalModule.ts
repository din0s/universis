import {NgModule} from '@angular/core';
import { RouterModalComponent } from './RouterModalComponent';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';

@NgModule({
  declarations: [RouterModalComponent],
  imports: [
    RouterModule,
    CommonModule
  ],
  providers: [
  ],
  exports: [RouterModalComponent]
})
export class RouterModalModule {
  constructor() {
    //
  }
}
